package be.foreach.bamboo.services;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.builder.LifeCycleState;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.execution.DeploymentContext;
import com.atlassian.bamboo.deployments.execution.service.DeploymentExecutionService;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.deployments.results.service.DeploymentResultService;
import com.atlassian.bamboo.deployments.versions.DeploymentVersion;
import com.atlassian.bamboo.deployments.versions.service.DeploymentVersionService;
import com.atlassian.bamboo.exception.WebValidationException;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.trigger.TriggerManager;
import com.atlassian.bamboo.resultsummary.ImmutableResultsSummary;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import com.opensymphony.xwork2.Action;
import org.acegisecurity.context.SecurityContextHolder;

import java.util.List;

/**
 * @author niels
 * @since 22/01/14
 */
public class DeploymentActionServiceImpl implements DeploymentActionService {

    private static final String JSON_KEY_RUNNING_TIME = "runningTime";
    private static final String JSON_KEY_ENVIRONMENT_ID = "environmentId";
    private static final String JSON_KEY_DEPLOYMENT_VERSION_ID = "deploymentVersionId";
    private static final String JSON_KEY_MESSAGE = "message";

    private final DeploymentProjectService deploymentProjectService;
    private final DeploymentExecutionService deploymentExecutionService;
    private final DeploymentVersionService deploymentVersionService;
    private final CachedPlanManager planManager;
    private final TriggerManager triggerManager;
    private final DeploymentResultService deploymentResultService;
    private final BuildLoggerManager buildLoggerManager;

    public DeploymentActionServiceImpl( CachedPlanManager planManager, TriggerManager triggerManager, DeploymentProjectService deploymentProjectService, DeploymentExecutionService deploymentExecutionService, DeploymentVersionService deploymentVersionService, DeploymentResultService deploymentResultService, BuildLoggerManager buildLoggerManager ) {
        this.deploymentResultService = deploymentResultService;
        this.deploymentProjectService = deploymentProjectService;
        this.deploymentExecutionService = deploymentExecutionService;
        this.deploymentVersionService = deploymentVersionService;
        this.buildLoggerManager = buildLoggerManager;
        this.planManager = planManager;
        this.triggerManager = triggerManager;
    }

    @Override
    public JSONObject pollDeployment( long environmentId, long deploymentVersionId ) throws JSONException {
        try {
            DeploymentResult result = deploymentResultService.getLatestDeploymentResult( environmentId, deploymentVersionId );
            if ( result != null ) {
                JSONObject resultJson = new JSONObject();
                switch ( result.getLifeCycleState() ) {
                    case IN_PROGRESS:
                    case PENDING:
                    case QUEUED:
                        resultJson.put( JSON_KEY_RESULT, result.getLifeCycleState().toString() );
                        resultJson.put( JSON_KEY_ENVIRONMENT_ID, environmentId );
                        return resultJson;
                    case NOT_BUILT:
                        return createErrorResult( result.getReasonSummary() );
                    case FINISHED:
                        resultJson.put( JSON_KEY_RESULT, LifeCycleState.FINISHED.toString() );
                        if ( result.getFinishedDate() != null && result.getExecutedDate() != null ) {
                            resultJson.put( JSON_KEY_RUNNING_TIME, ( result.getFinishedDate().getTime() - result.getExecutedDate().getTime() ) );
                        } else {
                            // couldn't determine
                            resultJson.put( JSON_KEY_RUNNING_TIME, -1 );
                        }
                        return resultJson;
                    default:
                        resultJson.put( "message", "unknown lifecycle state... retry polling?" );
                        return resultJson;
                }
            } else {
                return createErrorResult( "No result found for this environment[" + environmentId + "] and deploymentVersion[" + deploymentVersionId + "]" );
            }
        } catch ( JSONException e ) {
            return createErrorResult( "Failed to retrieve last known request information" );
        }
    }

    @Override
    public JSONObject createDeployment( String planKey, long deploymentProjectId, long environmentId ) throws JSONException, WebValidationException {
        ImmutablePlan plan = planManager.getPlanByKey( PlanKeys.getPlanKey( planKey ) );
        if ( plan != null ) {
            ImmutableResultsSummary resultsSummary = plan.getLatestResultsSummary();
            if ( resultsSummary != null ) {
                PlanResultKey planResultKey = plan.getLatestResultsSummary().getPlanResultKey();
                try {
                    // Authorize any and all calls during processing of this request
                    SecurityContextHolder.getContext().setAuthentication( BambooPermissionManager.SYSTEM_AUTHORITY );
                    DeploymentVersion deploymentVersion = deploymentVersionService.createDeploymentVersion( deploymentProjectId, planResultKey );

                    DeploymentProject deploymentProject = deploymentProjectService.getDeploymentProjectForEnvironment( environmentId );
                    if ( deploymentProject != null ) {
                        List<? extends Environment> environments = deploymentProject.getEnvironments();

                        TriggerReason triggerReason = triggerManager.getTriggerReason( ManualBuildTriggerReason.KEY,
                                ImmutableMap.of( ManualBuildTriggerReason.TRIGGER_MANUAL_USER, "One Click Deployer" )
                        );
                        Environment environment = null;
                        for ( Environment env : environments ) {
                            if ( env.getId() == environmentId ) {
                                environment = env;
                                break;
                            }
                        }
                        if ( environment == null ) {
                            return createErrorResult( "No valid environment found on this deployment project: " + deploymentProject.getName() );
                        }
                        DeploymentContext deploymentContext = deploymentExecutionService.prepareDeploymentContext( environment, deploymentVersion, triggerReason );
                        deploymentExecutionService.execute( deploymentContext );
                        JSONObject resultJson = new JSONObject();
                        resultJson.put( JSON_KEY_RESULT, LifeCycleState.PENDING.toString() );
                        resultJson.put( JSON_KEY_ENVIRONMENT_ID, environment.getId() );
                        resultJson.put( JSON_KEY_DEPLOYMENT_VERSION_ID, deploymentVersion.getId() );
                        return resultJson;
                    } else {
                        return createErrorResult( "No deployment project found for environment: " + environmentId );
                    }
                } finally {
                    // reset authorization
                    SecurityContextHolder.getContext().setAuthentication( null );
                }
            } else {
                return createErrorResult( "No result summary found for plan: " + planKey );
            }
        } else {
            return createErrorResult( "No plan found for key: " + planKey );
        }
    }

    @Override
    public JSONObject logToDeployment( String logFragment, long environmentId, long deploymentVersionId ) throws JSONException {
        DeploymentResult result = deploymentResultService.getLatestDeploymentResult( environmentId, deploymentVersionId );
        JSONObject json;
        if ( result != null ) {
            BuildLogger logger = buildLoggerManager.getLogger( result.getKey() );
            for ( String logLine : logFragment.split( "\n" ) ) {
                logger.addBuildLogEntry( logLine );
            }
            logger.close();
            json = new JSONObject();
            json.put( "result", Action.SUCCESS );
        } else {
            json = createErrorResult( "No Deployment result for environment [" + environmentId + "] and deploymentVersion [" + deploymentVersionId + "]" );
        }
        return json;
    }

    public JSONObject createErrorResult( String message ) throws JSONException {
        JSONObject result = new JSONObject();
        result.put( JSON_KEY_RESULT, JSON_RESULT_ERROR );
        result.put( JSON_KEY_MESSAGE, message );
        return result;
    }
}
