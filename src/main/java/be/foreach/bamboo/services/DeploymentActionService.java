package be.foreach.bamboo.services;

import com.atlassian.bamboo.exception.WebValidationException;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;

/**
 * @author niels
 * @since 22/01/14
 */
public interface DeploymentActionService {

    String JSON_KEY_RESULT = "result";
    String JSON_RESULT_ERROR = "Error";

    JSONObject createDeployment( String planKey, long deploymentProjectId, long environmentId ) throws JSONException, WebValidationException;
    JSONObject pollDeployment( long environmentId, long deploymentVersionId ) throws JSONException;
    JSONObject logToDeployment( String logFragment, long environmentId, long deploymentVersionId ) throws JSONException;
    JSONObject createErrorResult( String message ) throws JSONException;

}
