package be.foreach.bamboo;

import be.foreach.bamboo.services.DeploymentActionService;
import com.atlassian.bamboo.exception.WebValidationException;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.jetbrains.annotations.NotNull;

import javax.servlet.http.HttpServletRequest;

public class CreateAndExecuteDeployment extends BambooActionSupport {

    private static final String REQUEST_PARAM_ACTION = "action";
    private static final String REQUEST_PARAM_ENVIRONMENT_ID = "environmentId";
    private static final String REQUEST_PARAM_DEPLOYMENT_VERSION_ID = "deploymentVersionId";
    private static final String REQUEST_PARAM_DEPLOYMENT_PROJECT_ID = "deploymentProjectId";
    private static final String REQUEST_PARAM_PLAN_KEY = "planKey";
    private static final String REQUEST_PARAM_LOG_FRAGMENT = "logFragment";

    private static final String ACTION_POLL_RESULT = "pollResult";
    private static final String ACTION_CREATE_DEPLOYMENT = "createDeployment";
    private static final String ACTION_APPEND_TO_BUILD_LOGGER = "appendLog";

    private final DeploymentActionService deploymentActionService;

    private JSONObject requestResult;

    public CreateAndExecuteDeployment( DeploymentActionService deploymentActionService ) {
        this.deploymentActionService = deploymentActionService;
    }

    @Override
    public String doDefault() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String action = request.getParameter( REQUEST_PARAM_ACTION );
        if ( StringUtils.isNotBlank( action ) ) {
            if ( ACTION_CREATE_DEPLOYMENT.equals( action ) ) {
                return doCreateDeployment( request );
            } else if ( ACTION_POLL_RESULT.equals( action ) ) {
                return doPoll( request );
            } else if ( ACTION_APPEND_TO_BUILD_LOGGER.equals( action ) ) {
                return doAppendToLog( request );
            }
        }
        setRequestResult( deploymentActionService.createErrorResult( "Unknown action: " + action ) );
        return ERROR;
    }

    private String doCreateDeployment( HttpServletRequest request ) throws JSONException, WebValidationException {
        String planKey = request.getParameter( REQUEST_PARAM_PLAN_KEY );
        long deploymentProjectId;
        long environmentId;
        try {
            deploymentProjectId = Long.parseLong( request.getParameter( REQUEST_PARAM_DEPLOYMENT_PROJECT_ID ) );
        } catch ( NumberFormatException nfe ) {
            setRequestResult( deploymentActionService.createErrorResult( "Invalid deployment project id: " + request.getParameter( REQUEST_PARAM_DEPLOYMENT_PROJECT_ID ) ) );
            return ERROR;
        }
        try {
            environmentId = Long.parseLong( request.getParameter( REQUEST_PARAM_ENVIRONMENT_ID ) );
        } catch ( NumberFormatException nfe ) {
            setRequestResult( deploymentActionService.createErrorResult( "Invalid environment id: " + request.getParameter( REQUEST_PARAM_ENVIRONMENT_ID ) ) );
            return ERROR;
        }
        setRequestResult( deploymentActionService.createDeployment( planKey, deploymentProjectId, environmentId ) );
        return getResult();
    }

    private String doPoll( HttpServletRequest request ) throws JSONException {
        long environmentId;
        long deploymentVersionId;
        try {
            environmentId = Long.parseLong( request.getParameter( REQUEST_PARAM_ENVIRONMENT_ID ) );
        } catch ( NumberFormatException nfe ) {
            setRequestResult( deploymentActionService.createErrorResult( "Invalid environment id: " + request.getParameter( REQUEST_PARAM_ENVIRONMENT_ID ) ) );
            return ERROR;
        }
        try {
            deploymentVersionId = Long.parseLong( request.getParameter( REQUEST_PARAM_DEPLOYMENT_VERSION_ID ) );
        } catch ( NumberFormatException nfe ) {
            setRequestResult( deploymentActionService.createErrorResult( "Invalid deployment version id: " + request.getParameter( REQUEST_PARAM_DEPLOYMENT_VERSION_ID ) ) );
            return ERROR;
        }
        setRequestResult( deploymentActionService.pollDeployment( environmentId, deploymentVersionId ) );
        return getResult();
    }

    private String doAppendToLog( HttpServletRequest request ) throws JSONException {
        long environmentId;
        long deploymentVersionId;
        String logFragment = request.getParameter( REQUEST_PARAM_LOG_FRAGMENT );
        try {
            environmentId = Long.parseLong( request.getParameter( REQUEST_PARAM_ENVIRONMENT_ID ) );
        } catch ( NumberFormatException nfe ) {
            setRequestResult( deploymentActionService.createErrorResult( "Invalid environment id: " + request.getParameter( REQUEST_PARAM_ENVIRONMENT_ID ) ) );
            return ERROR;
        }
        try {
            deploymentVersionId = Long.parseLong( request.getParameter( REQUEST_PARAM_DEPLOYMENT_VERSION_ID ) );
        } catch ( NumberFormatException nfe ) {
            setRequestResult( deploymentActionService.createErrorResult( "Invalid deployment version id: " + request.getParameter( REQUEST_PARAM_DEPLOYMENT_VERSION_ID ) ) );
            return ERROR;
        }
        if ( StringUtils.isBlank( logFragment ) ) {
            setRequestResult( deploymentActionService.createErrorResult( "Empty log fragment" ) );
            return ERROR;
        }
        setRequestResult( deploymentActionService.logToDeployment( logFragment, environmentId, deploymentVersionId ) );
        return getResult();
    }

    private String getResult() {
        try {
            if ( requestResult.getString( DeploymentActionService.JSON_KEY_RESULT ).equals( DeploymentActionService.JSON_RESULT_ERROR ) ) {
                return ERROR;
            }
        } catch ( JSONException e ) {
            return ERROR;
        }
        return SUCCESS;
    }

    @NotNull
    @Override
    public JSONObject getJsonObject() {
        return getRequestResult();
    }

    public JSONObject getRequestResult() {
        return requestResult;
    }

    public void setRequestResult( JSONObject requestResult ) {
        this.requestResult = requestResult;
    }
}